#!/usr/bin/env python
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal
import numpy as np
x, y = np.mgrid[-10:10:.01, -10:10:.01]
pos = np.empty(x.shape + (2,))
# print (pos)
pos[:, :, 0] = x; pos[:, :, 1] = y
# rv = multivariate_normal([0.5, -0.2], [[2.0, 0.3], [0.3, 0.5]])
rv = multivariate_normal(mean=[1,1], cov=[[1,0],[0,1]])
rv1  = multivariate_normal(mean=[1,1], cov=[[1,0],[0,1]])
# rv2  = multivariate_normal(mean=[3,4], cov=[[1,0],[0,1]])
# rv3  = multivariate_normal(mean=[-2,-6], cov=[[1,0],[0,1]])
# print (rv.pdf(pos))

# plt.contourf(x, y, rv.pdf(pos)+rv1.pdf(pos)+rv2.pdf(pos)+rv3.pdf(pos))
rv_total = rv.pdf(pos)+rv1.pdf(pos)
plt.contourf(x, y, rv_total)
total_cost = (sum(map(sum, rv_total)))
print(total_cost)
print ((rv.pdf(pos))[1,1])
fig1 = plt.figure()
ax = fig1.add_subplot(111)
ax.plot(x, y)
plt.show()
