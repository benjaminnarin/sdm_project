#!/usr/bin/env python
from pdf_function import *
import random

def genpoint(valuemin,maxvalue):
    x_point = [random.randint(valuemin, maxvalue) for x in xrange(1)] #Random Generation Of Points
    y_point = [random.randint(valuemin, maxvalue) for x in xrange(1)] #Random Generation Of Points
    return((x_point[0],y_point[0]))

# point_list = [(1,2),(-1,3),(2,1),(1,4)]
point_list = []
world = 10 #-10x10 world
valuemin = -10 #Generation of random points
maxvalue = 10 #Generation of random points
nvalues = 50 #Generation of random points
x_point = [random.randint(valuemin, maxvalue) for x in xrange(nvalues)] #Random Generation Of Points
y_point = [random.randint(valuemin, maxvalue) for x in xrange(nvalues)] #Random Generation Of Points
for num,point in enumerate(x_point): #Point Generation
    point_list.append((point,y_point[num])) #Point Generation
# print(point_list)

reduce_point = genpoint(valuemin,maxvalue)

worldgen = createWorld(world)
pdf = pdf_create(worldgen[2],point_list) #call to pdf function
reduced_pdf = reducepoint(worldgen[2],reduce_point,pdf)
norm_pdf = normalizePDF(reduced_pdf)
plotpdf(worldgen[0],worldgen[1],pdf)
plotpdf(worldgen[0],worldgen[1],reduced_pdf)
plotpdf(worldgen[0],worldgen[1],norm_pdf)
