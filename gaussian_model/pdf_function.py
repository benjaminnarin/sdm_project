#!/usr/bin/env python
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal
import numpy as np

#Pass in world, points

def createWorld(world_dim):
    x, y = np.mgrid[-world_dim:world_dim:.01, -world_dim:world_dim:.01]
    pos = np.empty(x.shape + (2,))
    pos[:, :, 0] = x; pos[:, :, 1] = y
    returnvars = [x,y,pos]
    return returnvars

def pdf_create(pos,point_list):
    rv_array = []
    # x_points = [] #attempt to plot points
    # y_points = [] #attempt to plot points
    for n in point_list:
        rv = multivariate_normal(mean=[n[0],n[1]], cov=[[1,0],[0,1]])
        # x_points.append(n[0]) #attempt to plot points
        # y_points.append(n[1]) #attempt to plot points
        rv_array.append(rv.pdf(pos))

    #Sum Over all pds
    rv_total = sum(rv_array)
    return(rv_total) #To access a point use rv_total[x,y]

def plotpdf(x,y,rv):
    # print(rv_total)
    plt.contourf(x,y,rv)

    # fig1 = plt.figure()
    # plt.scatter(x_points, y_points)
    # ax = fig1.add_subplot(111)
    # ax.plot(x, y)
    # ax.scatter(x_points, y_points) Plotting points does not work
    plt.show()

def reducepoint(pos,point,rv):
    reduce_rv = multivariate_normal(mean=[point[0],point[1]], cov=[[1,0],[0,1]])

    #reduction step
    rv_array = reduce_rv.pdf(pos)
    reduce_rv_pdf = (rv_array*-1)
    reduced_rv = (reduce_rv_pdf + rv)
    return(reduced_rv)

def normalizePDF(rv):
    total_cost = (sum(map(sum, rv)))
    print(total_cost)
    div_array = np.ones(rv.shape)
    div_array = div_array*total_cost
    # print(div_array)
    # print(rv_total)
    norm_rv = np.divide(rv,div_array)
    total_cost = (sum(map(sum, norm_rv)))
    print(total_cost)
    return(norm_rv)
